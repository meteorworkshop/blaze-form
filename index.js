import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';

checkNpmVersions({ 'simpl-schema': '1.5.5', classnames: '2.2.6' }, 'blaze-form');

import './components/form';
