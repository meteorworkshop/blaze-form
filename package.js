Package.describe({
  name: 'blaze-form',
  version: '0.1.2',
  summary: 'Beautiful form for blaze',
  git: 'https://bitbucket.org/meteorworkshop/blaze-form.git',
  documentation: 'README.md',
});

Package.onUse((api) => {
  api.versionsFrom('1.8');
  api.use([
    'ecmascript',
    'random',
    'templating',
    'template-controller',
    'tmeasday:check-npm-versions',
  ]);
  api.mainModule('index.js', 'client');
});
