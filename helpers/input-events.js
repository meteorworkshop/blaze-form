const inputEvents = (Instance) => {
  Instance.addEvents({
    'change input'(e) {
      const { onChange } = this.props;
      if (onChange) {
        e.preventDefault();
        onChange(e.currentTarget.value, e.currentTarget);
      }
    },
    'input input'(e) {
      const { onInput } = this.props;
      if (onInput) {
        e.preventDefault();
        onInput(e.currentTarget.value, e.currentTarget);
      }
    },
  });
};

export default inputEvents;
