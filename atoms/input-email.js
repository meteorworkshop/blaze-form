import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';
import { inputEvents } from '../helpers';

// import template
import './input-email.html';

const INPUT_EMAIL = 'BlazeInputEmail';

@inputEvents
@template(INPUT_EMAIL)
class BlazeInputEmail extends TemplateClass {
  props = new SimpleSchema({
    placeholder: { type: String, optional: true },
  }).extend(InputSchema);

  helpers = {
    attributes() {
      const {
        onChange, onInput, className, blazeForm, ...rest
      } = this.props;

      return {
        class: `${className || ''} form-control`,
        type: 'email',
        ...rest,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {};
}

export default INPUT_EMAIL;
