import { TemplateClass, template } from 'meteor/template-controller';

import { InputSchema } from '../schemas';

// import template
import './input-checkbox.html';

const INPUT_CHECKBOX = 'BlazeInputCheckbox';

@template(INPUT_CHECKBOX)
class BlazeInputCheckbox extends TemplateClass {
  props = InputSchema.omit('onInput').extend({
    label: String,
  });

  helpers = {
    attributes() {
      const {
        onChange, className, value, blazeForm, ...rest
      } = this.props;

      return {
        class: `${className || ''} form-check-input`,
        type: 'checkbox',
        checked: !!value,
        ...rest,
      };
    },
  };

  events = {
    'change input'(e) {
      const { onChange } = this.props;
      if (onChange) {
        e.preventDefault();
        onChange(e.currentTarget.checked, e.currentTarget);
      }
    },
  };
}

export default INPUT_CHECKBOX;
