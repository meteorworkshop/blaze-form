import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';

// import template
import './input-datalist.html';

const INPUT_DATALIST = 'BlazeInputDatalist';

@template(INPUT_DATALIST)
class BlazeInputDatalist extends TemplateClass {
  props = new SimpleSchema({
    placeholder: { type: String, optional: true },
    options: Array,
    'options.$': new SimpleSchema({
      title: String,
      subtitle: { type: String, optional: true },
      value: String,
    }),
  }).extend(InputSchema);

  helpers = {
    attributes() {
      const {
        options,
        onChange,
        onInput,
        className,
        value,
        blazeForm,
        ...rest
      } = this.props;
      const option = options.find(o => o.value === value) || { title: '' };

      return {
        class: `${className || ''} form-control`,
        type: 'text',
        list: `list-${rest.id}`,
        value: option.title,
        ...rest,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'change input'(e) {
      const { onChange, options } = this.props;
      if (onChange) {
        e.preventDefault();
        const { value } = e.currentTarget;
        const option = options.find(o => o.title === value) || { value };
        onChange(option.value, e.currentTarget, option);
      }
    },
  };
}

export default INPUT_DATALIST;
