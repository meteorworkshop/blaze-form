import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { inputEvents } from '../helpers';
import { InputSchema } from '../schemas';

// import template
import './input-number.html';

const INPUT_Number = 'BlazeInputNumber';

@inputEvents
@template(INPUT_Number)
class BlazeInputNumber extends TemplateClass {
  props = new SimpleSchema({
    max: { type: Number, optional: true },
    min: { type: Number, optional: true },
    step: { type: Number, optional: true },
    placeholder: { type: String, optional: true },
  }).extend(InputSchema);

  helpers = {
    attributes() {
      const {
        onChange, onInput, className,blazeForm, ...rest
      } = this.props;

      return {
        class: `${className || ''} form-control`,
        type: 'number',
        ...rest,
      };
    },
  };
}

export default INPUT_Number;
