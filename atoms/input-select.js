import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';

// import template
import './input-select.html';

const INPUT_SELECT = 'BlazeInputSelect';

@template(INPUT_SELECT)
class BlazeInputSelect extends TemplateClass {
  props = new SimpleSchema({
    options: Array,
    'options.$': new SimpleSchema({
      title: String,
      value: { type: String, optional: true },
    }),
  }).extend(InputSchema.omit('onInput'));

  helpers = {
    attributes() {
      const {
        options,
        onChange,
        onInput,
        className,
        value,
        blazeForm,
        ...rest
      } = this.props;

      return {
        class: `${className || ''} form-control`,
        ...rest,
      };
    },
    optionAttributes({ value }) {
      return {
        value,
        selected: value === this.props.value,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'change select'(e) {
      const { onChange, options } = this.props;
      if (onChange) {
        e.preventDefault();
        const { value } = e.currentTarget;
        const option = options.find(o => o.value === value);
        onChange(value, e.currentTarget, option);
      }
    },
  };
}

export default INPUT_SELECT;
