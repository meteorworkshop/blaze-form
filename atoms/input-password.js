import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';
import { inputEvents } from '../helpers';

// import template
import './input-password.html';

const INPUT_PASSWORD = 'BlazeInputPassword';

@inputEvents
@template(INPUT_PASSWORD)
class BlazeInputPassword extends TemplateClass {
  props = new SimpleSchema({
    placeholder: { type: String, optional: true },
  }).extend(InputSchema);

  helpers = {
    attributes() {
      const {
        onChange, onInput, className,blazeForm, ...rest
      } = this.props;

      return {
        class: `${className || ''} form-control`,
        type: 'password',
        ...rest,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {};
}

export default INPUT_PASSWORD;
