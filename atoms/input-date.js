import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';

// import template
import './input-date.html';

const INPUT_DATE = 'BlazeInputDate';

@template(INPUT_DATE)
class BlazeInputDate extends TemplateClass {
  props = new SimpleSchema({
    max: { type: Date, optional: true },
    min: { type: Date, optional: true },
  }).extend(InputSchema.omit('onInput'));

  /**
   * get ISO date
   * @param {Date} date
   * @return {string}
   */
  getDate(date) {
    const day = (`0${date.getDate()}`).slice(-2);
    const month = (`0${date.getMonth() + 1}`).slice(-2);
    const year = date.getFullYear();
    return `${year}-${month}-${day}`;
  }

  helpers = {
    attributes() {
      const {
        onChange,
        onInput,
        className,
        value,
        min,
        max,
        blazeForm,
        ...rest
      } = this.props;
      const isoValue = value && this.getDate(value);
      const isoMin = min && this.getDate(min);
      const isoMax = max && this.getDate(max);

      return {
        class: `${className || ''} form-control`,
        type: 'date',
        // for safari preview
        placeholder: new Date().toLocaleDateString(),
        value: isoValue,
        min: isoMin,
        max: isoMax,
        ...rest,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'change input'(e) {
      const { onChange } = this.props;
      if (onChange) {
        e.preventDefault();
        onChange(new Date(`${e.currentTarget.value} 00:00`), e.currentTarget);
      }
    },
  };
}

export default INPUT_DATE;
