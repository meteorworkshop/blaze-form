import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';

// import template
import './input-radio.html';

const INPUT_RADIO = 'BlazeInputRadio';

@template(INPUT_RADIO)
class BlazeInputRadio extends TemplateClass {
  props = new SimpleSchema({
    options: Array,
    'options.$': new SimpleSchema({
      title: String,
      value: String,
    }),
  }).extend(InputSchema.omit('onInput'));

  helpers = {
    attributes(option) {
      const {
        options,
        onChange,
        onInput,
        className,
        value,
        id,
        blazeForm,
        ...rest
      } = this.props;

      return {
        class: `${className || ''} form-check-input`,
        type: 'radio',
        id: `${id}-${option.value}`,
        value: option.value,
        checked: value === option.value,
        ...rest,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'change input'(e) {
      const { onChange, options } = this.props;
      if (onChange) {
        e.preventDefault();
        const { value } = e.currentTarget;
        const option = options.find(o => o.value === value);
        onChange(value, e.currentTarget, option);
      }
    },
  };
}

export default INPUT_RADIO;
