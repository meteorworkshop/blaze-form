import { TemplateClass, template } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import { InputSchema } from '../schemas';

// import template
import './input-multiple.html';

@template('BlazeInputMultipleItem')
class BlazeInputMultipleItem extends TemplateClass {
  props = new SimpleSchema({
    content: String,
    onClick: Function,
  });

  events = {
    'click button'() {
      this.props.onClick();
    },
  };
}

const INPUT_MULTIPLE = 'BlazeInputMultiple';

@template(INPUT_MULTIPLE)
class BlazeInputMultiple extends TemplateClass {
  props = new SimpleSchema({
    placeholder: { type: String, optional: true },
    options: { type: Array, optional: true },
    'options.$': new SimpleSchema({
      title: String,
      subtitle: { type: String, optional: true },
      value: String,
    }),
  }).extend(InputSchema).extend({
    value: { type: Array, defaultValue: [] },
    'value.$': String,
  });

  selectOption = {
    keydown: false,
    changed: false,
  };

  state = {
    selected: [],
  };

  onCreated() {
    this.autorun(() => {
      const { value } = this.props;
      if (value) {
        this.setState({ selected: value });
      }
    });
  }

  helpers = {
    attributes() {
      const {
        options,
        onChange,
        onInput,
        className,
        value,
        blazeForm,
        ...rest
      } = this.props;

      return {
        class: `${className || ''} form-control`,
        type: 'text',
        list: `list-${rest.id}`,
        value: '',
        ...rest,
      };
    },
    options() {
      const { selected } = this.state;
      const { options = [] } = this.props;
      return options.filter(o => !selected.includes(o.value));
    },
    selected() {
      const { selected } = this.state;
      const { options = [], onChange } = this.props;
      const selectedOptions = options.filter(o => selected.includes(o.value));
      return selectedOptions.map(option => ({
        content: option.title,
        onClick: () => {
          const value = selected.filter(v => v !== option.value);
          onChange(value, null, option);
          this.setState({ selected: value });
        },
      }));
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'input input'(e) {
      const { onInput } = this.props;
      if (onInput) {
        e.preventDefault();
        onInput(e.currentTarget.value, e.currentTarget);
      }
    },
    'keydown input'(e) {
      if (e.key) {
        return;
      }
      this.selectOption.keydown = true;
    },
    'change input'(e) {
      if (this.selectOption.keydown) {
        this.selectOption.changed = true;
      }
    },
    'keyup input'(e) {
      const { keydown, changed } = this.selectOption;
      if (e.key || !keydown || !changed) {
        return;
      }
      const { selected } = this.state;
      const { onChange, options } = this.props;
      if (onChange) {
        e.preventDefault();
        const option = options.find(o => o.title === e.currentTarget.value);
        e.currentTarget.value = '';
        selected.push(option.value);
        onChange(selected, e.currentTarget, option);
        this.setState({ selected });
        this.selectOption.keydown = false;
        this.selectOption.changed = false;
      }
    },
  };
}

export default INPUT_MULTIPLE;
