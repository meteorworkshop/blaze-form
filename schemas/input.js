import SimpleSchema from 'simpl-schema';

const InputSchema = new SimpleSchema({
  name: { type: String },
  id: { type: String },
  className: { type: String, optional: true },
  // placeholder: { type: String, optional: true },
  value: { type: SimpleSchema.oneOf(Array, Date, String, Number, Boolean, Object), optional: true },
  'value.$': { type: SimpleSchema.oneOf(Date, String, Number, Boolean, Object), optional: true },
  onChange: { type: Function, optional: true },
  onInput: { type: Function, optional: true },
  disabled: { type: Boolean, optional: true },
  // there is no blazeForm prop, when we use only inputs
  blazeForm: { type: Function, label: 'Get form instance', optional: true },
  // different inputs have different options
  props: { type: Object, optional: true, blackbox: true },
  autocomplete: {
    type: String,
    optional: true,
    allowedValues: [ 'on', 'off' ],
  },
});

export default InputSchema;
