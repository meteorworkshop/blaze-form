import SimpleSchema from 'simpl-schema';

import { FormSchema } from '.';

const FormGroupSchema = FormSchema.pick('horizontal', 'size', 'hr', 'className')
  .extend(new SimpleSchema({
    id: { type: String, optional: true },
    help: { type: String, optional: true },
    label: { type: String, optional: true },
    checkbox: { type: Boolean, defaultValue: false },
    hr: { type: Boolean, defaultValue: false },
  }));

export default FormGroupSchema;
