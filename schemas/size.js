import SimpleSchema from 'simpl-schema';

const SizeSchema = new SimpleSchema({
  lg: { type: Number, optional: true, min: 1, max: 12 },
  md: { type: Number, optional: true, min: 1, max: 12 },
  sm: { type: Number, optional: true, min: 1, max: 12 },
  xs: { type: Number, optional: true, min: 1, max: 12 },
});

export default SizeSchema;
