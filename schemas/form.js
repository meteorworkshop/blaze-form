import SimpleSchema from 'simpl-schema';

import { SizeSchema } from '.';

const FormSchema = new SimpleSchema({
  horizontal: { type: Boolean, defaultValue: false },
  autocomplete: {
    type: String,
    defaultValue: 'on',
    allowedValues: ['on', 'off'],
  },
  inline: { type: Boolean, defaultValue: false },
  hr: { type: Boolean, optional: true },
  name: { type: String, optional: true },
  fields: { type: Array, optional: true },
  'fields.$': SimpleSchema.oneOf(
    String,
    {
      type: new SimpleSchema({
        template: { type: String, optional: true },
        field: { type: String },
        props: { type: Object, blackbox: true, optional: true },
      }),
    },
  ),
  document: { type: Object, blackbox: true, optional: true },
  schema: { type: SimpleSchema, blackbox: true, optional: true },
  className: { type: String, optional: true },
  onSubmit: { type: Function, optional: true },
  handleForm: { type: Function, optional: true },
  size: { type: SizeSchema, defaultValue: {} },
  submit: {
    type: SimpleSchema.oneOf(
      new SimpleSchema({
        content: { type: String, defaultValue: 'Готово' },
        color: { type: String, defaultValue: 'primary' },
        block: { type: Boolean, defaultValue: false },
        className: { type: String, optional: true },
      }),
      Boolean,
    ),
    defaultValue: {},
  },
});

export default FormSchema;
