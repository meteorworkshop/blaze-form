import { template, TemplateClass } from 'meteor/template-controller';
import c from 'classnames';

import { FormGroupSchema } from '../schemas';

// import template
import './form-group.html';

@template('BlazeFormGroup')
class BlazeFormGroup extends TemplateClass {
  props = FormGroupSchema;

  helpers = {
    formGroupClass() {
      const {
        className,
        horizontal: h,
        checkbox,
        size: {
          lg, md, sm, xs,
        },
      } = this.props;
      return c({
        'form-group': true,
        'form-check': checkbox,
        [`col-lg-${lg}`]: !h && lg,
        [`col-md-${md}`]: !h && md,
        [`col-sm-${sm}`]: !h && sm,
        [`col-xs-${xs}`]: !h && xs,
        [className]: !!className,
      });
    },
    hasLabel() {
      const { checkbox, label } = this.props;
      return !checkbox && label;
    },
    labelSizeClass() {
      const {
        horizontal: h, size: {
          lg, md, sm, xs,
        },
      } = this.props;
      return c({
        'control-label': true,
        [`col-lg-${12 - lg}`]: h && lg,
        [`col-md-${12 - md}`]: h && md,
        [`col-sm-${12 - sm}`]: h && sm,
        [`col-xs-${12 - xs}`]: h && xs,
      });
    },
    inputSizeClass() {
      const {
        horizontal: h, size: {
          lg, md, sm, xs,
        },
      } = this.props;
      return c({
        [`col-lg-${lg}`]: h && lg,
        [`col-md-${md}`]: h && md,
        [`col-sm-${sm}`]: h && sm,
        [`col-xs-${xs}`]: h && xs,
      });
    },
  };
}
