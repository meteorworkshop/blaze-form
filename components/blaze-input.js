import { template, TemplateClass } from 'meteor/template-controller';
import { Template } from 'meteor/templating';

import { FormGroupSchema, InputSchema } from '../schemas';

import './form-group';
import {
  INPUT_TEXT,
  INPUT_NUMBER,
  INPUT_EMAIL,
  INPUT_PASSWORD,
  INPUT_DATE,
  INPUT_RADIO,
  INPUT_DATALIST,
  INPUT_CHECKBOX,
  INPUT_MULTIPLE,
  INPUT_SELECT,
} from '../atoms';
import './blaze-input.html';

@template('BlazeInput')
class BlazeInput extends TemplateClass {
  props = FormGroupSchema.omit('checkbox').extend(InputSchema).extend({
    groupClassName: { type: String, optional: true },
    type: { type: String },
  });

  templateMap = {
    text: INPUT_TEXT,
    number: INPUT_NUMBER,
    email: INPUT_EMAIL,
    password: INPUT_PASSWORD,
    date: INPUT_DATE,
    radio: INPUT_RADIO,
    datalist: INPUT_DATALIST,
    checkbox: INPUT_CHECKBOX,
    multiple: INPUT_MULTIPLE,
    select: INPUT_SELECT,
    // schema field types
    string: INPUT_TEXT,
    boolean: INPUT_CHECKBOX,
    array: INPUT_MULTIPLE,
  };

  helpers = {
    formGroup() {
      const { type, groupClassName, ...rest } = this.props;
      return {
        checkbox: type === 'checkbox' || type === 'boolean',
        ...rest,
        className: groupClassName,
      };
    },
    attributes() {
      const {
        type, props, onChange, name, ...rest
      } = this.props;

      return {
        name,
        ...rest,
        ...props,
        onChange: (value) => {
          onChange(name, value);
        },
      };
    },
    template() {
      const { type } = this.props;
      if (this.templateMap[type]) {
        return this.templateMap[type];
      }
      if (Template[type]) {
        return type;
      }
      return this.templateMap.text;
    },
  };

  events = {};
}
