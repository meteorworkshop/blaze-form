import { Match } from 'meteor/check';
import { Random } from 'meteor/random';
import { template, TemplateClass } from 'meteor/template-controller';
import c from 'classnames';

import { FormSchema } from '../schemas';

import './blaze-input';
import './submit';
import './form.html';

@template('BlazeForm')
class BlazeForm extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = FormSchema;

  // Setup private reactive template state
  state = {
    document: {},
  };

  _resetCallbacks = [];

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    const { document } = this.props;
    if (document) {
      this.state.setState({ document });
    }
  }

  onRendered() {
    // Вернем DOM узел формы и функцию для получения данных с формы
    if (this.props.handleForm) {
      const form = this.find('form');
      this.props.handleForm({
        form,
        triggerSubmit: () => this.$(form).trigger('submit'),
        getData: () => this.state.document,
      });
    }
  }

  onDestroyed() {
  }

  onReset(cb) {
    this._resetCallbacks.push(cb);
  }

  // Helpers work like before but <this> is always the template instance!
  helpers = {
    attributes() {
      const {
        className, horizontal, inline, name, autocomplete,
      } = this.props;
      return {
        class: c({
          'form-horizontal': horizontal,
          'form-inline': inline,
          [`${className}`]: !!className,
        }),
        id: name,
        autocomplete,
      };
    },
    fieldSettings(field) {
      return this.settings(field);
    },
    submitData() {
      const { submit, size, horizontal } = this.props;
      return { size, horizontal, ...submit };
    },
    hasSubmit() {
      return this.props.submit;
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'submit form'(e) {
      e.preventDefault();
      if (this.props.onSubmit) {
        this.props.onSubmit(e.target, this.state.document);
      }
    },
    'reset form'() {
      this.state.document = {};
      this._resetCallbacks.forEach((cb) => {
        cb.call();
      });
    },
  };

  settings(field) {
    const {
      schema, horizontal, document, size, hr,
    } = this.props;
    const settings = {
      horizontal,
      size,
      hr,
    };
    if (Match.test(field, String)) {
      settings.name = field;
      settings.type = schema.getQuickTypeForKey(field);
      settings.id = Random.id();
      settings.onChange = (...args) => this.handleChange(...args);
    }
    if (Match.test(field, Object)) {
      const {
        template: type,
        field: name,
        props: {
          id = Random.id(),
          help,
          label,
          className,
          groupClassName,
          disabled,
          onChange,
          hr: fieldHr = hr,
          ...rest
        } = {},
      } = field;
      settings.hr = fieldHr;
      settings.name = name;
      settings.type = type || schema.getQuickTypeForKey(name);
      settings.id = id;
      settings.help = help;
      settings.label = label;
      settings.disabled = disabled;
      settings.className = className;
      settings.groupClassName = groupClassName;
      settings.onChange = onChange ? (...ars) => {
        this.handleChange(...ars);
        onChange(...ars);
      } : (...args) => this.handleChange(...args);
      settings.props = rest;
      // if the field has a value set through props params nor from the document
      this.setDefaultValue(settings.name, rest.value);
    }
    settings.value = (document && this.getValue(document, settings.name)) || '';
    settings.label = this.label(settings.name, settings);

    settings.blazeForm = () => this;

    return settings;
  }

  setDefaultValue(field, value) {
    const { document } = this.props;
    if (document || !value) {
      return;
    }

    if (!this.getValue(this.state.document, field)) {
      this.handleChange(field, value);
    }
  }

  handleChange(field, value) {
    const { document } = this.state;
    this.setValue(document, field, value);
    this.setState({ document });
  }

  label(field, settings) {
    const { schema } = this.props;
    if (settings.label === false) {
      return;
    }

    return settings.label || (schema && schema.label(field)) || '';
  }

  getValue(doc, field) {
    const [name, ...other] = field.split('.');
    if (!other.length || !doc[name]) {
      return doc[name];
    }
    return this.getValue(doc[name], other.join('.'));
  }

  setValue(doc, field, value) {
    const [name, ...other] = field.split('.');
    if (!other.length) {
      doc[name] = value;
      return;
    }
    if (!doc[name]) {
      doc[name] = {};
    }
    this.setValue(doc[name], other.join('.'), value);
  }
}
