import { template, TemplateClass } from 'meteor/template-controller';
import c from 'classnames';

import { FormSchema } from '../schemas';

import './submit.html';

@template('BlazeSubmit')
class BlazeSubmit extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = FormSchema.getObjectSchema('submit')
    .extend(FormSchema.pick('size', 'horizontal'));

  prefix = 'btn';

  // Helpers work like before but <this> is always the template instance!
  helpers = {
    size() {
      const { size: { lg = 10, md = 9, sm, xs }, horizontal } = this.props;
      return (horizontal && c({
        [`col-lg-${lg}`]: !!lg,
        [`col-md-${md}`]: !!md,
        [`col-sm-${sm}`]: !!sm,
        [`col-xs-${xs}`]: !!xs,
        [`col-lg-offset-${12 - lg}`]: !!lg,
        [`col-md-offset-${12 - md}`]: !!md,
        [`col-sm-offset-${12 - sm}`]: !!sm,
        [`col-xs-offset-${12 - xs}`]: !!xs,
      })) || '';
    },
    attributes() {
      const { color, block, className } = this.props;
      return {
        class: c({
          [className]: !!className,
          [this.prefix]: true,
          [`${this.prefix}-${color}`]: !!color,
          [`${this.prefix}-block`]: block,
        }),
        type: 'submit',
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {};

}
